package live.wecancode.employee.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer //Mandatory to enable the eureka server
public class EmployeeTrackerEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeTrackerEurekaServerApplication.class, args);
	}

}
