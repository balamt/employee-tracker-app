package live.wecancode.employee.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class EmployeeTrackerApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeTrackerApiGatewayApplication.class, args);
	}

}
