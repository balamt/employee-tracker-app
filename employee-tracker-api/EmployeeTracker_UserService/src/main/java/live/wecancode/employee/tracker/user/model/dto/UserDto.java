package live.wecancode.employee.tracker.user.model.dto;

import lombok.Data;

@Data
public class UserDto {
    long userId;
    String name;
    String emailid;
    String userType;
}
