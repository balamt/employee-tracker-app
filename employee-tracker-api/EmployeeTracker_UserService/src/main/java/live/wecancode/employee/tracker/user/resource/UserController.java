package live.wecancode.employee.tracker.user.resource;

import live.wecancode.employee.tracker.user.model.UserResponse;
import live.wecancode.employee.tracker.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/status")
    public ResponseEntity status(){
        return ResponseEntity.ok("Up and running");
    }

    @GetMapping("/all")
    public ResponseEntity getUsers(){
        UserResponse response = new UserResponse();
        response.setUsers(userService.getAllUsers());
        response.setCount(response.getUsers().size());
        response.setMessage("SUCCESS");
        return ResponseEntity.ok(response);
    }
}
