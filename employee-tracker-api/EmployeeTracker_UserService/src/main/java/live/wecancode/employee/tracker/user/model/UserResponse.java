package live.wecancode.employee.tracker.user.model;

import live.wecancode.employee.tracker.user.model.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse implements Serializable {
    List<UserDto> users;
    int count;
    String errorCode;
    String message;
}
