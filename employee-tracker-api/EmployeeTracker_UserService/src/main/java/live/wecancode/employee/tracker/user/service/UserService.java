package live.wecancode.employee.tracker.user.service;

import live.wecancode.employee.tracker.user.model.User;
import live.wecancode.employee.tracker.user.model.dto.UserDto;
import live.wecancode.employee.tracker.user.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModelMapper modelMapper;

    public List<UserDto> getAllUsers() {
        //Stream to use map and modelMapper to convert from entity to dto class then collect convert to list
        return userRepository.findAll().stream().map(user -> modelMapper.map(user, UserDto.class))
                        .collect(Collectors.toList());
    }
}
